﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace timespan_serialization
{
	public class Settings : IXmlSerializable
	{
		public XmlSchema GetSchema()
		{
			return null;
		}

		public void ReadXml(XmlReader reader)
		{
			string element = null;
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
					element = reader.Name;
				else if (reader.NodeType == XmlNodeType.Text)
				{
					if (element != null && fieldSetters.ContainsKey(element))
						fieldSetters[element](this, reader.Value);
				}
			}
		}

		public void WriteXml(XmlWriter writer)
		{
			writer.WriteElementString(inboxElementName, Inbox);
			writer.WriteElementString(outboxElementName, Outbox);
			writer.WriteElementString(intervalElementName, ((int)Interval.TotalSeconds).ToString());
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((Settings)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (Inbox != null ? Inbox.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (Outbox != null ? Outbox.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ Interval.GetHashCode();
				return hashCode;
			}
		}

		[XmlElement("Inbox")]
		public string Inbox;

		[XmlElement("Outbox")]
		public string Outbox;

		[XmlElement("IntervalInSeconds")]
		public TimeSpan Interval;

		protected bool Equals(Settings other)
		{
			return string.Equals(Inbox, other.Inbox) && string.Equals(Outbox, other.Outbox) && Interval.Equals(other.Interval);
		}

		private static readonly string inboxElementName = XmlElementAttributeGetter.GetXmlElementAttributeOrMemberName((Settings s) => s.Inbox);
		private static readonly string outboxElementName = XmlElementAttributeGetter.GetXmlElementAttributeOrMemberName((Settings s) => s.Outbox);
		private static readonly string intervalElementName = XmlElementAttributeGetter.GetXmlElementAttributeOrMemberName((Settings s) => s.Interval);

		private static readonly IDictionary<string, Action<Settings, string>> fieldSetters = new Dictionary<string, Action<Settings, string>>
		{
			{inboxElementName, (settings, value) => settings.Inbox = value},
			{outboxElementName, (settings, value) => settings.Outbox = value},
			{intervalElementName, (settings, value) =>
				settings.Interval = TimeSpan.FromSeconds(double.Parse(value.Replace(',', '.'), CultureInfo.InvariantCulture))}
		};
	}
}
