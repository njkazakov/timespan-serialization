﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml.Serialization;

namespace timespan_serialization
{
	public class XmlElementAttributeGetter
	{
		public static string GetXmlElementAttributeOrMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
		{
			MemberInfo memberInfo = ((MemberExpression)memberAccess.Body).Member;
			var attributes = memberInfo.GetCustomAttributes(typeof(XmlElementAttribute), false);
			XmlElementAttribute attrubute = null;
			if (attributes.Length > 0)
				attrubute = (XmlElementAttribute)attributes[0];
			return attrubute != null ? attrubute.ElementName : memberInfo.Name;
		}
	}
}
