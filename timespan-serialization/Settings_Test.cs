﻿using System;
using System.IO;
using System.Xml.Serialization;
using NUnit.Framework;

namespace timespan_serialization
{
	[TestFixture]
	public class Settings_Test
	{
		[Test]
		public void Serialize()
		{
			var msettings = new Settings
			{
				Interval = TimeSpan.FromMinutes(5),
				Inbox = "inbox",
				Outbox = "outbox"
			};

			var serializer = new XmlSerializer(typeof (Settings));
			string serialized;
			using (var writer = new StringWriter())
			{
				serializer.Serialize(writer, msettings);
				serialized = writer.ToString();
			}

			var expected = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <Inbox>inbox</Inbox>
  <Outbox>outbox</Outbox>
  <IntervalInSeconds>300</IntervalInSeconds>
</Settings>";
			Assert.AreEqual(expected, serialized);
		}

		public class Deserialize
		{
			[Test]
			public void Serialized()
			{
				var settings = new Settings
				{
					Interval = TimeSpan.FromMinutes(5),
					Inbox = "inbox",
					Outbox = "outbox"
				};
				var serializer = new XmlSerializer(typeof (Settings));
				string serialized;
				using (var writer = new StringWriter())
				{
					serializer.Serialize(writer, settings);
					serialized = writer.ToString();
				}
				TextReader textReader = new StringReader(serialized);

				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual(settings, deserialized);
			}

			[Test]
			public void DisorderedElements()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <Outbox>outbox</Outbox>
  <IntervalInSeconds>300</IntervalInSeconds>
  <Inbox>inbox</Inbox>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual("inbox", deserialized.Inbox);
				Assert.AreEqual("outbox", deserialized.Outbox);
				Assert.AreEqual(TimeSpan.FromMinutes(5), deserialized.Interval);
			}

			[Test]
			public void MissingElement()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <Inbox>inbox</Inbox>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual("inbox", deserialized.Inbox);
				Assert.IsNull(deserialized.Outbox);
				Assert.AreEqual(TimeSpan.Zero, deserialized.Interval);
			}

			[Test]
			public void ExtraElement()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <Inbox>inbox</Inbox>
  <Outbox>outbox</Outbox>
  <IntervalInSeconds>300</IntervalInSeconds>
  <Extra>extra</Extra>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual("inbox", deserialized.Inbox);
				Assert.AreEqual("outbox", deserialized.Outbox);
				Assert.AreEqual(TimeSpan.FromMinutes(5), deserialized.Interval);
			}

			[Test]
			public void IncorrectXml_ThrowsInvalidOperationException()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <Inbox>inbox
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				Assert.Throws<InvalidOperationException>(() => serializer.Deserialize(textReader));
			}

			[Test]
			public void FractionWithDot()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <IntervalInSeconds>10.5</IntervalInSeconds>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual(TimeSpan.FromSeconds(10.5), deserialized.Interval);
			}

			[Test]
			public void NonnumericalIntervalValue_ThrowsInvalidOperationException()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <IntervalInSeconds>nonnumerical</IntervalInSeconds>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				Assert.That(() => serializer.Deserialize(textReader),
					Throws.TypeOf<InvalidOperationException>()
						.With.InnerException.TypeOf<FormatException>());
			}

			[Test]
			public void FractionWithComma()
			{
				var input = @"<?xml version=""1.0"" encoding=""utf-16""?>
<Settings>
  <IntervalInSeconds>10,5</IntervalInSeconds>
</Settings>";
				var serializer = new XmlSerializer(typeof (Settings));
				TextReader textReader = new StringReader(input);
				var deserialized = (Settings) serializer.Deserialize(textReader);

				Assert.AreEqual(TimeSpan.FromSeconds(10.5), deserialized.Interval);
			}
		}
	}
}