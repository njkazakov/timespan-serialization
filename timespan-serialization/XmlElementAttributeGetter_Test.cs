﻿using System.Xml.Serialization;
using NUnit.Framework;

namespace timespan_serialization
{
	[TestFixture]
	public class XmlElementAttributeGetter_Test
	{
		[Test]
		public void FieldWithXmlElementAttribute_ReturnsAttributeValue()
		{
			Assert.AreEqual("ElementName", XmlElementAttributeGetter.GetXmlElementAttributeOrMemberName((ClassForTest c) => c.FieldWithAttribute));
		}

		[Test]
		public void FieldWithoutXmlElementAttribute_ReturnsFieldName()
		{
			Assert.AreEqual("FieldWithoutAttribute", XmlElementAttributeGetter.GetXmlElementAttributeOrMemberName((ClassForTest c) => c.FieldWithoutAttribute));
		}

		private class ClassForTest
		{
			private ClassForTest(string fieldWithoutAttribute, int fieldWithAttribute)
			{
				FieldWithoutAttribute = fieldWithoutAttribute;
				FieldWithAttribute = fieldWithAttribute;
			}

			public readonly string FieldWithoutAttribute;

			[XmlElement("ElementName")]
			public int FieldWithAttribute;
		}
	}
}
